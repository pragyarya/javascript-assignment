var c=document.getElementById("circles");
var ctx=c.getContext("2d");

var coords = [ [60,60], [150,150], [250,250],[350,350],[450,450] ];

for(var i = 0; i < coords.length; i++){
    ctx.beginPath();
    ctx.arc(coords[i][0], coords[i][1], 58, 0, Math.PI * 2, true);
    ctx.stroke();
    ctx.fillStyle = "rgba(96,41, 110,"+(i+1)*0.2+" )";
    ctx.fill();
    ctx.closePath();
}
